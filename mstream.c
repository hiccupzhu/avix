/*
 * mstream.c
 *
 *  Created on: Mar 13, 2015
 *      Author: szhu
 */

#include <vlc_common.h>
#include <vlc_stream.h>
#include <assert.h>
#include "openssl/des.h"

#include "mstream.h"

#define MALIGN_SIZE (8)
#define MALIGN(x) (((x)+(MALIGN_SIZE-1))&(~(MALIGN_SIZE-1)))
#define CLIPHER_NOT

static int dec_DES(uint8_t* data, int len)
{
    assert(data != NULL);
    assert((len % MALIGN_SIZE) == 0);

    DES_key_schedule ks;
    const_DES_cblock block;
    uint8_t tmp[8];

    memcpy(&block, "0$NIGjXF", 8);
    DES_set_key_unchecked((const_DES_cblock*)block, &ks);

    for(uint8_t* p = data; p < data + len; p += 8){
        memset(tmp, 0, 8);
        DES_ecb_encrypt((const_DES_cblock *)p, (DES_cblock *)tmp, &ks, DES_DECRYPT);
        memcpy(p, tmp, 8);
    }

    return 0;
}

/**
 * Store in pp_peek a pointer to the next "i_peek" bytes in the stream
 * \return The real number of valid bytes. If it's less
 * or equal to 0, *pp_peek is invalid.
 * \note pp_peek is a pointer to internal buffer and it will be invalid as
 * soons as other stream_* functions are called.
 * \note Contrary to stream_Read, stream_Peek doesn't modify the stream
 * position, and doesn't necessarily involve copying of data. It's mainly
 * used by the modules to quickly probe the (head of the) stream.
 * \note Due to input limitation, the return value could be less than i_peek
 * without meaning the end of the stream (but only when you have i_peek >=
 * p_input->i_bufsize)
 */
int mstream_Peek( stream_t *s, const uint8_t **pp_peek, int i_peek )
{
    static uint8_t buf[256];

    uint64_t pos = mstream_Tell(s);

    int delt0 = pos & (MALIGN_SIZE-1);
    int sz = MALIGN(i_peek + delt0);
    int delt1 = sz - i_peek - delt0;

    if(delt0 > 0){
        mstream_Seek(s, pos - delt0);
    }


    int ret = s->pf_peek( s, pp_peek, sz );

    memcpy(buf, *pp_peek, ret);

#if 0
    uint8_t *p = buffer;
    for(int i = 0; i < ret; i ++){
        p[i] = ~p[i];
    }
#else
    dec_DES(buf, sz);
#endif

    if(delt0 > 0){
        mstream_Seek(s, pos);
    }

    *pp_peek = buf + delt0;

    return (ret - delt0 - delt1);
}


/**
 * Try to read "i_read" bytes into a buffer pointed by "p_read".  If
 * "p_read" is NULL then data are skipped instead of read.
 * \return The real number of bytes read/skip. If this value is less
 * than i_read that means that it's the end of the stream.
 * \note mstream_Read increments the stream position, and when p_read is NULL,
 * this is its only task.
 */
int mstream_Read( stream_t *s, void *p_read, int i_read )
{
    if(p_read == NULL){
        return s->pf_read( s, p_read, i_read );
    }

    uint8_t *buf = NULL;
    uint64_t pos = mstream_Tell(s);


    //position align
    int delt0 = pos & (MALIGN_SIZE-1);
    //i_read align
    int sz = MALIGN(i_read + delt0);

    int delt1 = sz - i_read - delt0;

    if(delt0 > 0){
        mstream_Seek(s, pos - delt0);
    }

    buf = (uint8_t*)malloc(sz);


    int ret = s->pf_read( s, buf, sz );

    if(ret > 0){
        //////////////////////////////////////////////////
#if 0
        uint8_t *p = (uint8_t *)buf;

        for(int i = 0; i < ret; i ++){
            p[i] = ~p[i];
        }
#else
        dec_DES(buf, sz);
#endif

        ///////////////////////////////////////////////////
        memcpy(p_read, buf+delt0, i_read);

        mstream_Seek(s, pos + i_read);
        ret = (ret - delt0 - delt1);
    } else {
        //do nothing
    }

    free(buf);
    return ret;
}


/* Helpers */
uint32_t mstream_ReadU32( stream_t *s, void *p_read, uint32_t i_toread )
{
    uint32_t i_return = 0;
    if ( i_toread > INT32_MAX )
    {
        i_return = mstream_Read( s, p_read, INT32_MAX );
        if ( i_return < INT32_MAX )
            return i_return;
        else
            i_toread -= INT32_MAX;
    }
    i_return += mstream_Read( s, (uint8_t *)p_read + i_return, (int32_t) i_toread );
    return i_return;
}


/**
 * Read "i_size" bytes and store them in a block_t.
 * It always read i_size bytes unless you are at the end of the stream
 * where it return what is available.
 */
block_t *mstream_Block( stream_t *s, int i_size )
{
    if( i_size <= 0 ) return NULL;

    /* emulate block read */
    block_t *p_bk = block_Alloc( i_size );
    if( p_bk )
    {
        int i_read = mstream_Read( s, p_bk->p_buffer, i_size );
        if( i_read > 0 )
        {
            p_bk->i_buffer = i_read;
            return p_bk;
        }
        block_Release( p_bk );
    }
    return NULL;
}


/**
 * Get the current position in a stream
 */
int64_t mstream_Tell( stream_t *s )
{
    uint64_t i_pos;
    mstream_Control( s, STREAM_GET_POSITION, &i_pos );
    if( i_pos >> 62 )
        return (int64_t)1 << 62;
    return i_pos;
}


int mstream_Seek( stream_t *s, uint64_t i_pos )
{
    return mstream_Control( s, STREAM_SET_POSITION, i_pos );
}


/**
 * Get the size of the stream.
 */
int64_t mstream_Size( stream_t *s )
{
    uint64_t i_pos;
    mstream_Control( s, STREAM_GET_SIZE, &i_pos );
    if( i_pos >> 62 )
        return (int64_t)1 << 62;
    return i_pos;
}


int mstream_Control( stream_t *s, int i_query, ... )
{
    va_list args;
    int     i_result;

    if( s == NULL )
        return VLC_EGENERIC;

    va_start( args, i_query );
    i_result = s->pf_control( s, i_query, args );
    va_end( args );
    return i_result;
}









