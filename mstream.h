/*
 * mstream.h
 *
 *  Created on: Mar 13, 2015
 *      Author: szhu
 */

#ifndef MODULES_DEMUX_MP4X_MSTREAM_H_
#define MODULES_DEMUX_MP4X_MSTREAM_H_

#include <stdint.h>

int         mstream_Peek( stream_t *s, const uint8_t **pp_peek, int i_peek );

int         mstream_Read( stream_t *s, void *p_read, int i_read );
uint32_t    mstream_ReadU32( stream_t *s, void *p_read, uint32_t i_toread );
block_t *   mstream_Block( stream_t *s, int i_size );

int         mstream_Control( stream_t *s, int i_query, ... );
int64_t     mstream_Tell( stream_t *s );
int         mstream_Seek( stream_t *s, uint64_t i_pos );
int64_t     mstream_Size( stream_t *s );

#endif /* MODULES_DEMUX_MP4X_MSTREAM_H_ */
