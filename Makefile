TARGET=libavix_plugin.la

TOPDIR="/opt/source/vlc-2.2.0"
#get "access_udp" from "libaccess_udp_plugin.la" 
MODULE_NAME=`p="${TARGET}"; p="$${p\#\#*/}"; p="$${p\#lib}"; echo "$${p%_plugin*}"`

#$(warning =====${MODULE_NAME})

CC = i686-w64-mingw32-gcc -g
LIBTOOL = libtool
CFLAGS += -g -std=gnu99 -DHAVE_CONFIG_H
CFLAGS += -D__PLUGIN__ \
	-D_FILE_OFFSET_BITS=64 \
	-D_REENTRANT \
	-D__USE_MINGW_ANSI_STDIO=1
	#-D_THREAD_SAFE \
	
#CFLAGS  +=  -DMODULE_STRING="$(MODULE_NAME)"
CFLAGS  += -DMODULE_STRING=\"avix\"
CFLAGS  += -I. \
		   -I$(TOPDIR)/modules \
		   -I$(TOPDIR)/win32 \
		   -I$(TOPDIR)/modules/access \
		   -I$(TOPDIR)/modules/codec \
		   -I$(TOPDIR)/include \
		   -I$(TOPDIR)/win32/include \
		   -I$(TOPDIR)/contrib/i686-w64-mingw32/include 
		   
LDFLAGS += -rpath '/usr/lib/vlc/plugins/access' \
	-avoid-version -module -shared \
	-export-symbols-regex ^vlc_entry \
	-shrext .dll \
	-no-undefined \
	$(TOPDIR)/win32/compat/libcompat.la \
	$(TOPDIR)/win32/src/libvlccore.la \
	$(TOPDIR)/win32/modules/module.rc.lo \
	-Wc,--static -Wc,-static-libgcc \
	-Wl,--nxcompat -Wl,--no-seh -Wl,--dynamicbase \
	-L/opt/source/vlc-2.2.0/contrib/i686-w64-mingw32/lib \
	-L./openssl/lib \
	-lm -lz -leay32


$(TARGET):libavix.lo avix.lo mstream.lo
	#$(LIBTOOL) --tag=CC --mode=link $(CC) $(LDFLAGS) -o $@ $+
	./doltlibtool --tag=CC --mode=link $(CC) $(LDFLAGS) -o $@ $+

%.lo:%.c
	#$(LIBTOOL) --tag=CC --mode=compile $(CC) $(CFLAGS) -c -o $@ $<
	./doltcompile $(CC) $(CFLAGS) -MT $@ -MD -MP -c -o $@ $<
	
%.o:%.c
	$(CC) -std=gnu99 -I. -c -o $@ $<

	
install:
	cp .libs/libavix_plugin.dll /mnt/hgfs/E/temp/vlc-2.2.0/plugins/demux/

clean:
	rm -rf *.o *.lo *.a *.la .libs destest